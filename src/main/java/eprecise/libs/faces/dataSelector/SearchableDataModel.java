package eprecise.libs.faces.dataSelector;

import java.util.List;

public interface SearchableDataModel<T> {
    public List<T> query(String str);
}
