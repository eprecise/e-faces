
package eprecise.libs.faces.dateRange;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.NamingContainer;
import javax.faces.component.UIInput;
import javax.faces.component.UINamingContainer;
import javax.faces.component.html.HtmlInputText;


@FacesComponent("dateRange")
@ResourceDependencies({ @ResourceDependency(library = "dateRange", name = "dateRange.css"), @ResourceDependency(library = "dateRange", name = "dateRange.js"),
        @ResourceDependency(library = "js", name = "moment-with-locales.min.js"), @ResourceDependency(library = "js", name = "jquery-ui.min.js"),
        @ResourceDependency(library = "css", name = "jquery-ui.min.css"), @ResourceDependency(library = "css", name = "jquery.comiseo.daterangepicker.css"),
        @ResourceDependency(library = "js", name = "jquery.comiseo.daterangepicker.min.js") })
public class DateRange extends UIInput implements NamingContainer {

    private HtmlInputText input;

    @Override
    public String getFamily() {
        return UINamingContainer.COMPONENT_FAMILY;
    }

    @Override
    public Object getSubmittedValue() {
        return input.getSubmittedValue();
    }

    public HtmlInputText getInput() {
        return input;
    }

    public void setInput(HtmlInputText input) {
        this.input = input;
    }

}
